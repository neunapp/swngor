# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Package, Route, Property

#
class PackageAdmin(admin.ModelAdmin):
   list_display = (
       'name',
       'days',
       'short_description',
       'price',
       'published',
       'tipo',
   )
   search_fields = ('name',)
   list_filter = ('price','days','published', 'tipo',)
   #campos para agregar
   fields =(
        'name',
        'image',
        'short_description',
        'resume',
        'days',
        'city_origin',
        'city_destination',
        'price',
        'tipo',
        'tags',
        'published',
        'visit',
   )
   #solo si hay many to many
   filter_horizontal = ('tags',)


class RouteAdmin(admin.ModelAdmin):
   list_display = (
       'order',
       'package',
       'destination',
   )
   search_fields = ('package__name',)
   list_filter = ('package','destination',)


class PropertyAdmin(admin.ModelAdmin):
   list_display = (
       'order',
       'contenido',
       'route',
   )
   search_fields = ('route__package__name',)
   list_filter = ('route',)


admin.site.register(Package, PackageAdmin)
admin.site.register(Route, RouteAdmin)
admin.site.register(Property, PropertyAdmin)
