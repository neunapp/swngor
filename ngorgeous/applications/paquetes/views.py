# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy, reverse

from django.views.generic import (
    CreateView,
    UpdateView,
    TemplateView,
    DeleteView,
    DetailView,
    FormView,
    ListView
)

from django.views.generic.edit import FormMixin

#
from .models import Package, Property, Route
#
from .forms import SearchForm
#
from .functions import rutas_por_paquete

#vistas para blog
class FilterPackageView(ListView):
    """ filtro para Package"""

    context_object_name = 'paquetes'
    template_name = 'paquetes/list.html'

    def get_context_data(self, **kwargs):
        context = super(FilterPackageView, self).get_context_data(**kwargs)
        context['form'] = SearchForm
        return context

    def get_queryset(self):
        #recuperamos el valor por GET
        q = self.request.GET.get("destino1", '')
        r = self.request.GET.get("destino2", '')
        queryset = Package.objects.search_package(q, r)
        return queryset


class PackageDetailView(DetailView):
    model = Package
    template_name = 'paquetes/detail.html'

    def get_context_data(self, **kwargs):
        context = super(PackageDetailView, self).get_context_data(**kwargs)
        context['rutas'] = rutas_por_paquete(self.get_object().pk)
        context['paquetes'] = Package.objects.list_feature()
        return context
