# -*- coding: utf-8 -*-
from django import forms

#
class SearchForm(forms.Form):
   '''
   formulario para buscar paquetes
   '''

   destino1 = forms.CharField(
       max_length='100',
       required=False,
       widget=forms.TextInput(
           attrs={
                'class':'tours__banner__copy__ctrls__form__input',
                'placeholder': 'Origen'
           }
       )
   )
   destino2 = forms.CharField(
       max_length='100',
       required=False,
       widget=forms.TextInput(
           attrs={
                'class':'tours__banner__copy__ctrls__form__input',
                'placeholder': 'Destino'
           }
       )
   )
