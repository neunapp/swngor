from django.db import models
from django.db.models import F, Q

class PackageManager(models.Manager):
    """procedimiento para tabla package"""

    def list_feature(self):
        """lista paquetes mas destacados"""

        return self.order_by('name', '-visit')[:6]

    def search_package(self, destino1, destino2):
        """buscar paquetes por medio de destinos"""
        if ((destino1 == '') and (destino2 == '')):
            #devolvemos las mas vsitadas
            consulta = self.order_by('name', '-visit')
        else:
            #devolvemos las mas vsitadas
            consulta = self.filter(
                Q(city_origin__icontains=destino1) | Q(city_destination__icontains=destino2)
            ).order_by('name')

        return consulta


class RouteManager(models.Manager):
    """procedimiento para tabla route"""

    def search_package(self, destino1, destino2):
        """buscar paquetes por medio de destinos"""
        if ((destino1 == '') and (destino2 == '')):
            #devolvemos las mas vsitadas
            consulta = self.order_by('package__name', '-package__visit').distinct('package__name')
        else:
            #devolvemos las mas vsitadas
            consulta = self.filter(
                Q(destination__name__icontains=destino1) | Q(destination__name__icontains=destino2)
            ).order_by('package__name').distinct('package__name')

        return consulta

    def list_destinations(self, package):
        """lista de destinos por paquete"""

        return self.filter(
            package__published=True,
            package__pk=package,
        ).order_by('order')


class PropertyManager(models.Manager):
    """procedimiento para tabla propiedades de rutas"""

    def list_propertys(self, route):
        """lista las propiedades de una ruta"""

        return self.filter(
            route__pk=route,
        ).order_by('order')
