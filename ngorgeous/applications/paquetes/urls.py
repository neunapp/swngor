# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from . import views

app_name="paquetes_app"

urlpatterns = [
    #url para formulario de contacto
    url(r'^tours-y-paquetes-turisticos-por-cusco-puno-arequipa-peru/$',
        views.FilterPackageView.as_view(),
        name='paquetes_list'
    ),
    #url para ver el detalle de un tour
    url(r'^(?P<city>[-\w]+)/(?P<slug>[-\w]+)/$',
        views.PackageDetailView.as_view(),
        name='paquetes-detail'
    ),
]
