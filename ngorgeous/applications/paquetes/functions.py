# -*- coding: utf-8 -*-

from .models import Route, Property

#functions
class RutasPaquete():
    route = None
    propiedades = None


def rutas_por_paquete(paquete):
    """ funcion que lista rutas de un paquete y propiedades de una ruta """

    resultado = []
    rutas = Route.objects.list_destinations(paquete)
    if rutas:
        #recuperamos la propiedades
        for r in rutas:
            rp = RutasPaquete()
            rp.route = r
            rp.propiedades = Property.objects.list_propertys(r.pk)
            #
            resultado.append(rp)

    return resultado
