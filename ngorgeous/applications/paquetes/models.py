# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.template.defaultfilters import slugify
#third import
from datetime import datetime, timedelta
from model_utils.models import TimeStampedModel
#import manager
from .managers import PackageManager, RouteManager, PropertyManager


@python_2_unicode_compatible
class Package(TimeStampedModel):
    """ modelo para paquetes y tours """

    TYPE_CHOICES = (
        ('0', 'Tour'),
        ('1', 'Paquete'),
    )
    name = models.CharField('nombre', max_length=100)
    slogan = models.CharField('Frase', max_length=100)
    image = models.ImageField('imagen', upload_to="paquetes")
    resume = models.TextField('resumen')
    days = models.IntegerField('dias')
    visit = models.IntegerField('visita', default=0)
    short_description = models.TextField('descripcion corta')
    price = models.IntegerField('precio')
    city_origin = models.CharField('Ciudad origen', max_length=100, blank=True)
    city_destination = models.CharField('Ciudad destino', max_length=100, blank=True)
    tags = models.ManyToManyField('miscelanea.Tag', blank=True)
    published = models.BooleanField('Publicado', default=False)
    tipo = models.CharField('Tipo', max_length=1, choices=TYPE_CHOICES)
    slug = models.SlugField(editable=False, max_length=200)
    objects = PackageManager()

    class Meta:
        verbose_name = 'paquete turistico'
        verbose_name_plural = 'paquetes turisticos'
        ordering = ['-created']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.id:
            # calculamos el total de segundos de la hora actual
            now = datetime.now()
            total_time = timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second
            )
            seconds = int(total_time.total_seconds())
            slug_unique = '%s %s' % (self.name, str(seconds))
        else:
            seconds = self.slug.split('-')[-1]  # recuperamos los segundos
            slug_unique = '%s %s' % (self.name, str(seconds))

        self.slug = slugify(slug_unique)
        super(Package, self).save(*args, **kwargs)



@python_2_unicode_compatible
class Route(TimeStampedModel):

    order = models.IntegerField(default=0)
    package = models.ForeignKey(
        Package,
        verbose_name='paquete_turistico',
        on_delete=models.CASCADE,
    )
    destination = models.ForeignKey(
        'destinos.Destination',
        verbose_name='destino',
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    #
    objects = RouteManager()

    class Meta:
        verbose_name = 'ruta'
        verbose_name_plural = 'rutas'
        ordering = ['-created']

    def __str__(self):
        return str(self.destination.name) + '---' + str(self.package.name)



@python_2_unicode_compatible
class Property(TimeStampedModel):

    order = models.IntegerField('orden')
    contenido = models.CharField(blank=True, max_length=120)
    route = models.ForeignKey(
        Route,
        verbose_name='rutas',
        on_delete=models.CASCADE,
    )
    #
    objects = PropertyManager()

    class Meta:
        verbose_name = 'propiedad'
        verbose_name_plural = 'propiedades'
        ordering = ['-created']

    def __str__(self):
        return str(self.order)
