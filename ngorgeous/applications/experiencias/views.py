# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy, reverse

from django.views.generic import (
    CreateView,
    UpdateView,
    TemplateView,
    DeleteView,
    DetailView,
    FormView,
    ListView
)

#app paquete
from applications.paquetes.models import Package
#
from .models import Experience

#vistas para blog
class ExperienceListView(ListView):
    """ Lista las experiencias del guia"""

    context_object_name = 'experiencias'
    template_name = 'experiencias/index.html'

    def get_queryset(self):
        queryset = Experience.objects.filter(
            published=True
        ).order_by('-created')[:30]
        return queryset


class ExperienceDetailView(DetailView):
    model = Experience
    template_name = 'experiencias/detail.html'

    def get_context_data(self, **kwargs):
        context = super(ExperienceDetailView, self).get_context_data(**kwargs)
        context['paquetes'] = Package.objects.list_feature()
        return context
