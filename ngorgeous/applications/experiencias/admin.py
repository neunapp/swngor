# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
#
from .models import Experience


class ExperienceAdmin(admin.ModelAdmin):
   list_display = (
       'name',
       'guide',
       'created',
   )
   search_fields = ('name',)
   list_filter = ('guide',)
   #campos para agregar
   fields =(
        'name',
        'image',
        'video',
        'guide',
        'short_description',
        'guide_experience',
        'tags',
   )
   #solo si hay many to many
   filter_horizontal = ('tags',)

admin.site.register(Experience, ExperienceAdmin)
