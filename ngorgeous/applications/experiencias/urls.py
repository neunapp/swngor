# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from . import views

app_name="experiencias_app"

urlpatterns = [
    #url para formulario de contacto
    url(r'^viaje-por-el-peru-con-experiencias-inolvidables/$',
        views.ExperienceListView.as_view(),
        name='experiencias_index'
    ),
    #url para ver el detalle de un destino
    url(r'^viajes-por-peru/(?P<slug>[-\w]+)/(?P<pk>\d+)/$',
        views.ExperienceDetailView.as_view(),
        name='experiencias_detail'
    ),
]
