# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.template.defaultfilters import slugify
# third-party
from model_utils.models import TimeStampedModel
from datetime import datetime, timedelta
from colorfield.fields import ColorField

# Create your models here.

@python_2_unicode_compatible
class Home(TimeStampedModel):

    title = models.CharField('titulo', max_length=200, blank=True)
    subtitle = models.CharField('subtitulo', max_length=200, blank=True)
    coverpage = models.ImageField('portada', upload_to='home', blank=True, null=True)
    button_primary = models.CharField('boton primario', max_length=200, blank=True)
    button_secundary = models.CharField('boton secundario', max_length=200, blank=True)
    first_value = models.CharField(
        'Primera caracteristica de empresa',
        blank=True,
        max_length=30
    )
    first_value_content = models.CharField(
        'Texto primera caracteristica de empresa',
        blank=True,
        max_length=100
    )
    second_value = models.CharField(
        'Segunda caracteristica de empresa',
        blank=True,
        max_length=30
    )
    second_value_content = models.CharField(
        'Texto Segunda caracteristica de empresa',
        blank=True,
        max_length=100
    )
    third_value = models.CharField(
        'tercera caracteristica de empresa',
        blank=True,
        max_length=30
    )
    third_value_content = models.CharField(
        'Texto tercera caracteristica de empresa',
        blank=True,
        max_length=100
    )
    first_subtitle = models.CharField('Primer subtitulo', max_length=200, blank=True)
    first_subtitle_description = models.CharField('Descripcion del primer subtitulo', max_length=200, blank=True)
    second_subtitle = models.CharField('Segundo subtitulo', max_length=200, blank=True)
    second_subtitle_description = models.CharField('Descripcion del segundo subtitulo', max_length=200, blank=True)
    phone = models.CharField('Telefono', max_length=15, blank=True)
    tags = models.ManyToManyField('miscelanea.Tag')
    class Meta:
        verbose_name = 'Pagina Principal'
        verbose_name_plural = 'Pagina Principal'
        ordering = ['-created']

    def __str__(self):
        return self.title



@python_2_unicode_compatible
class Contact(TimeStampedModel):
    """ modelo para registrar contacto """

    email = models.EmailField('email')
    name = models.CharField('nombre', max_length=40, blank=True)
    phone = models.CharField('celular', max_length=30, blank=True)
    contry = models.CharField('pais', max_length=50, blank=True)
    hour_atention = models.CharField('hora de atencion', max_length= 40, blank=True)
    day_atention = models.CharField('dia de atencion', max_length= 40, blank=True)
    messagge = models.TextField('mensaje')

    class Meta:
        verbose_name = 'Mensaje'
        verbose_name_plural = 'Bandeja de Mensajes'
        ordering = ['-created']

    def __str__(self):

        return self.email


@python_2_unicode_compatible
class Client(TimeStampedModel):
    """ modelo para registrar contacto """

    email = models.EmailField('email')
    name = models.CharField('nombre', max_length=40)
    image = models.URLField('url de perfil facebook', blank=True, null=True)
    messagge = models.TextField('comentario',blank=True)
    class Meta:
        verbose_name = 'Comentario de cliente'
        verbose_name_plural = 'Comentarios de clientes'
        ordering = ['-created']

    def __str__(self):

        return self.name
