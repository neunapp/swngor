# -*- coding: utf-8 -*-
from __future__ import unicode_literals
#django library
from django.urls import reverse_lazy, reverse
from django.views.generic import (
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
    ListView,
    TemplateView,
    View,
)

#models paquetes
from applications.paquetes.models import Package
#models destinos
from applications.destinos.models import Destination

#models
from .models import Contact, Home, Client
#forms
from .forms import ContactForm, ContactHomeForm


class HomeView(CreateView):
    """  vista que muestra la pagina principal """

    model = Contact
    form_class = ContactHomeForm
    success_url = '.'
    template_name = 'home/index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        #contexto principal
        context['home'] = Home.objects.order_by('-created')[0]
        #contexto home para paquetes y toours
        context['paquetes'] = Package.objects.filter(
            published = True,
        ).order_by('-visit')[:6]
        # contecto para destinos mas populares
        context['destinos'] = Destination.objects.filter(
            published = True,
        ).order_by('-visit')[:6]
        # contecto para opiniones de los clnetes sobre la empresa
        context['opiniones'] = Client.objects.order_by('name')[:6]
        return context


class ContactCreateView(CreateView):
    model = Contact
    form_class = ContactForm
    success_url = '.'
    template_name = 'home/contacto.html'
