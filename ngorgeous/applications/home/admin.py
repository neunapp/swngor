# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Home, Contact, Client
#
admin.site.register(Home)
admin.site.register(Contact)
admin.site.register(Client)
