# -*- coding: utf-8 -*-
from django import forms

from .models import Contact


class ContactForm(forms.ModelForm):
    """
    formulario para contacto
    """

    class Meta:
        model = Contact
        fields = '__all__'
        widgets = {
            'email': forms.EmailInput(
                attrs={
                    'class':'contacto__copy__form__item__input',
                    'placeholder': 'E-mail',
                }
            ),
            'phone': forms.TextInput(
                attrs={
                    'class':'contacto__copy__form__item__input',
                    'placeholder': 'Telfono / Phone',
                }
            ),
            'contry': forms.TextInput(
                attrs={
                    'class':'contacto__copy__form__item__input',
                    'placeholder': 'Pais / Contry',
                }
            ),
            'name': forms.TextInput(
                attrs={
                    'class':'contacto__copy__form__item__input',
                    'placeholder': 'Nombres / Full name',
                }
            ),
            'hour_atention': forms.TextInput(
                attrs={
                    'class':'contacto__copy__form__item__input',
                    'placeholder': 'En que horario se le puede llamar',
                }
            ),
            'day_atention': forms.TextInput(
                attrs={
                    'class':'contacto__copy__form__item__input',
                    'placeholder': 'Que dias se le puede llamar',
                }
            ),
            'messagge': forms.Textarea(
                attrs={
                    'class':'contacto__copy__form__item__input',
                    'rows':'2',
                    'cols':'20',
                    'placeholder': 'mensaje / messagge',
                }
            ),
        }


class ContactHomeForm(forms.ModelForm):
    """
    formulario para contacto
    """

    class Meta:
        model = Contact
        fields = '__all__'
        widgets = {
            'email': forms.EmailInput(
                attrs={
                    'class':'home-contact__form__input',
                    'placeholder': 'E-mail',
                }
            ),
            'phone': forms.TextInput(
                attrs={
                    'class':'home-contact__form__input',
                    'placeholder': 'Telfono / Phone',
                }
            ),
            'contry': forms.TextInput(
                attrs={
                    'class':'home-contact__form__input',
                    'placeholder': 'Pais / Contry',
                }
            ),
            'name': forms.TextInput(
                attrs={
                    'class':'home-contact__form__input',
                    'placeholder': 'Nombres / Full name',
                }
            ),
            'hour_atention': forms.TextInput(
                attrs={
                    'class':'home-contact__form__input',
                    'placeholder': 'En que horario se le puede llamar',
                }
            ),
            'day_atention': forms.TextInput(
                attrs={
                    'class':'home-contact__form__input',
                    'placeholder': 'Que dias se le puede llamar',
                }
            ),
            'messagge': forms.Textarea(
                attrs={
                    'class':'home-contact__form__input',
                    'row':'3',
                    'placeholder': 'mensaje / messagge',
                }
            ),
        }
