# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

#
from .models import Destination, Gallery


class DestinationAdmin(admin.ModelAdmin):
   list_display = (
       'name',
       'title',
       'city',
       'visit',
       'published'
   )
   search_fields = ('title', 'name',)
   list_filter = ('city','published',)
   #campos para agregar
   fields =(
        'name',
        'title',
        'image',
        'city',
        'short_description',
        'content',
        'published',
        'visit',
        'tags',
   )
   #solo si hay many to many
   filter_horizontal = ('tags',)

admin.site.register(Destination, DestinationAdmin)
admin.site.register(Gallery)
