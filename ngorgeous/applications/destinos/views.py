# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy, reverse

from django.views.generic import (
    CreateView,
    UpdateView,
    TemplateView,
    DeleteView,
    DetailView,
    FormView,
    ListView
)

#appp paquetes
from applications.paquetes.models import Package
#
from .models import Destination

#vistas para blog
class DestinationListView(ListView):
    """ Lista de destinos mas populares """

    context_object_name = 'destinos'
    template_name = 'destinos/index.html'

    def get_queryset(self):
        queryset = Destination.objects.filter(
            published=True
        ).order_by('-visit')[:30]
        return queryset


class DestinationDetailView(DetailView):
    model = Destination
    template_name = 'destinos/detail.html'

    def get_context_data(self, **kwargs):
        context = super(DestinationDetailView, self).get_context_data(**kwargs)
        context['paquetes'] = Package.objects.list_feature()
        return context
