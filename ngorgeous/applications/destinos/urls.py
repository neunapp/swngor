# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from . import views

app_name="destinos_app"

urlpatterns = [
    #url para formulario de contacto
    url(r'^visita-lugares-en-cusco-arequipa-puno-y-todo-el-peru/$',
        views.DestinationListView.as_view(),
        name='destinos_index'
    ),
    #url para ver el detalle de un destino
    url(r'^destinos/(?P<city>[-\w]+)/(?P<slug>[-\w]+)/$',
        views.DestinationDetailView.as_view(),
        name='destinos_detail'
    ),
]
