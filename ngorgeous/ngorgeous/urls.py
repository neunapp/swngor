from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
#
#from django.contrib.sitemaps.views import sitemap

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #
    url(r'^', include('applications.home.urls')),
    #urls para aplicacion paquetes
    url(r'^', include('applications.paquetes.urls')),
    #urls para aplicacion destinos
    url(r'^', include('applications.destinos.urls')),
    #urls para aplicacion experiecnias
    url(r'^', include('applications.experiencias.urls')),
    #url for ckeditor
    url(r'^froala_editor/', include('froala_editor.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
